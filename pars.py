#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
from re import *
 
def main(url):
    a = requests.get(url).text
    patt = compile('"location":{[a-zA-Z ,a-яА-ЯЁё./\"():-;0123456789_]*"name":"[a-zA-Z ,a-яА-ЯЁё./\():;]*"')
    try:
        w = patt.findall(a)[0]
        w = (w.split(":")[-1][1:-1],w.split(":")[2].split(",")[0][1:-1])
        return w[0] +"|"+get_c(w[1])
        
    except IndexError:
        patt = compile('"addressLocality":"[a-zA-Z ,a-яА-ЯЁё./\():;]*"')
        patt_n = compile('"location":{"id":"[-+,_0123456789.,]*"')
        try :
            l = patt.findall(a)[0].split(":")[1]
            l = l[1:len(l)-1]
            try:
                ids = patt_n.findall(a)[0].split(":")[-1][1:-1]
                return l +"|"+get_c(ids)
                
            except IndexError:
                return l +"|can't find location|no"
        except IndexError:
            return 'no location found|no|no'
         

def get_c(ids):
    new_a = requests.get("https://www.instagram.com/explore/locations/"+ids+"/").text
    nt_patt = compile('latitude"[ ]*content="[-+,_0123456789.,]*"')
    nn_patt = compile('longitude"[ ]*content="[-+,_0123456789.,]*"')
    try :
        lat,lon = (nt_patt.findall(new_a)[0],nn_patt.findall(new_a)[0])
        return str(lat.split("=")[1][1:-1]) +"|"+ str(lon.split("=")[1][1:-1])
    except IndexError:
        return "can't find location|no"
    
   
   
