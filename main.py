import tornado.httpserver
import tornado.ioloop
import tornado.web
import pars as pr
import os
import json

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("instagram parser")
    def post(self):
        url = self.get_argument('url')
        if "https://www.instagram.com/p/" in url:
            res = pr.main(url).split("|")
            r = json.dumps({"location": res[0], "latitude": res[1], "longitude": res[2]})
            self.write(r)
            self.finish()
        else:
            self.write(json.dumps({"location": "bad url", "latitude": "no", "longitude": "no"}))
            self.finish()
        
def main():
    application = tornado.web.Application([
        (r"/", MainHandler),
    ])
    http_server = tornado.httpserver.HTTPServer(application)
    port = int(os.environ.get("PORT", 5000))
    http_server.listen(port)
    tornado.ioloop.IOLoop.instance().start()
 
if __name__ == "__main__":
    main()
